import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import PanelPage from './components/PanelPage';

class App extends Component {
  render() {
    return (
      <div className="App">
        <h1>Tendrils Control Panel</h1>
        <PanelPage />
      </div>
    );
  }
}

export default App;
