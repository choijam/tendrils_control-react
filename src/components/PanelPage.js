import React, { Component } from 'react';
import { Label, FormGroup, InputGroup, Button, ControlLabel, FormControl, HelpBlock, Checkbox, Radio, Panel } from 'react-bootstrap';


class PanelPage extends Component {
    constructor() {
        super()
        this.state = {
            autoFade: false
        }
    }
    handleAutoFade() {
        this.setState({
            autoFade: !this.state.autoFade
        })
    }
    changeVaryFlow(data) {
        console.log('VaryFlow is ', data.target.value)
    }
    render() {
        const { autoFade } = this.state;
        return (
            <div className='main-panel'>
                <Panel bsStyle="info">
                    <Panel.Heading ><span>Setting</span></Panel.Heading>
                    <Panel.Body>
                        <InputGroup>
                            <InputGroup.Addon>VaryFlow</InputGroup.Addon>
                            <FormControl type="text" onChange={(e) => this.changeVaryFlow(e)} />
                        </InputGroup>
                        <Button bsStyle={autoFade ? "success" : "warning"} onClick={() => this.handleAutoFade()}>AutoFade : {autoFade ? "on" : "off"}</Button>
                    </Panel.Body>
                </Panel>
                {/* <Panel>
                    <Panel.Heading>
                        <Panel.Title componentClass="h3">Panel heading with a title</Panel.Title>
                    </Panel.Heading>
                    <Panel.Body>Panel content</Panel.Body>
                </Panel> */}
            </div>
        );
    }
}

export default PanelPage;